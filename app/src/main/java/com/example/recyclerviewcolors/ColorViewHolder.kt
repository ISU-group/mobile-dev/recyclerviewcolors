package com.example.recyclerviewcolors

import android.view.View
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.recyclerviewcolors.databinding.ColorItemBinding

class ColorViewHolder(private val item: View): RecyclerView.ViewHolder(item) {
    private val binding = ColorItemBinding.bind(item)

    init {
        binding.root.setOnClickListener { _ ->
            Toast.makeText(
                binding.colorHex.context,
                binding.colorHex.text,
                Toast.LENGTH_LONG
            ).show()
        }
    }

    fun bind(color: Int) = with(binding) {
        colorHex.text = colorHex.context.getString(R.string.color_hex_template, color)
        colorTile.setBackgroundColor(color)
    }
}